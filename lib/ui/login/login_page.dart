import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/genLoginSignupHeader.dart';
import 'package:majootestcase/common/widget/genTextFormField.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/UserModel.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/movie/list.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/DatabaseHandler/DBHelper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:email_validator/email_validator.dart';



class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  Future<SharedPreferences> _pref = SharedPreferences.getInstance();
  final _formKey = new GlobalKey<FormState>();

  final _conUserId = TextEditingController();
  final _conUserEmail = TextEditingController();
  final _conPassword = TextEditingController();

  bool _isObscurePassword = true;
  var dbHelper;

  bool _isHidePassword = true;

  void _togglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  void _togglePasswordVisibility_c() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  @override
  void initState() {
    super.initState();
    dbHelper = DbHelper();
  }

  login() async {
    String uid = _conUserId.text;
    String email = _conUserEmail.text;
    String passwd = _conPassword.text;

    if (email.isEmpty) {
          Fluttertoast.showToast(
              msg: "Email Tidak Boleh kosong",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
    } else if (passwd.isEmpty) {
      Fluttertoast.showToast(
          msg: "Password Tidak Boleh Kosong",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      await dbHelper.getLoginUser(email, passwd).then((userData) {
        if (userData != null) {
          setSP(userData).whenComplete(() {
            Fluttertoast.showToast(
                msg: "Login Berhasil",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 1,
                backgroundColor: Colors.green,
                textColor: Colors.white,
                fontSize: 16.0);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) => MovieList()),
                    (Route<dynamic> route) => false);
          });
        } else {
          Fluttertoast.showToast(
              msg: "Akun Belum Terdaftar",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      }).catchError((error) {
        print(error);
        Fluttertoast.showToast(
            msg: "Error: Login Fail",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
    }
  }

  Future setSP(UserModel user) async {
    final SharedPreferences sp = await _pref;

    sp.setString("user_id", user.user_id);
    sp.setString("user_name", user.user_name);
    sp.setString("email", user.email);
    sp.setString("password", user.password);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if(state is AuthBlocLoggedInState){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                _form(),
                Container(
                  margin: EdgeInsets.all(30.0),
                  width: double.infinity,
                  child: FlatButton(
                    child: Text(
                      'Login',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: login,
                    // onPressed: () async {
                    //   Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //       builder: (_) => BlocProvider(
                    //         create: (context) => HomeBlocCubit()..fetching_data(),
                    //         child: HomeBlocScreen(),
                    //       ),
                    //     ),
                    //   );
                    // },
                  ),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          genLoginSignupHeader('Login'),
          getTextFormField(
              controller: _conUserEmail,
              icon: Icons.email,
              hintName: 'Email'),
          SizedBox(height: 10.0),
          getTextFormField(
            controller: _conPassword,
            icon: Icons.lock,
            hintName: 'Password',
            isObscureText: true,
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(
                           builder: (context) => SignupForm()),);
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  // void handleLogin() async {
  //   final _email = _emailController.value;
  //   final _password = _passwordController.value;
  //   if (formKey.currentState?.validate() == true &&
  //       _email != null &&
  //       _password != null
  //      ) {
  //
  //     AuthBlocCubit authBlocCubit = AuthBlocCubit();
  //     User user = User(
  //         email: _email,
  //         password: _password,
  //     );
  //     authBlocCubit.login_user(user);
  //   }
  // }
}
