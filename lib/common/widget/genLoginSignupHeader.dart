import 'package:flutter/material.dart';

class genLoginSignupHeader extends StatelessWidget {
  String headerName;

  genLoginSignupHeader(this.headerName);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 20.0),
          Image.asset(
            "assets/images/logo.png",
            height: 150.0,
            width: 150.0,
          ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }
}
